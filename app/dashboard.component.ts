import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Video } from './video';
import { VideoService } from './video.service';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  videos: Video[] = [];

  constructor(
    //Types for Instance
    private router: Router,
    private videoService: VideoService) {
  }
/*Loading the first two clips */
  ngOnInit(): void {
    this.videoService.getVideos()
      .then(videos => this.videos = videos.slice(1, 3));
  }

  /*Loading the first two clips*/
  gotoDetail(video: Video): void {
    let link = ['/detail', video.id];
    this.router.navigate(link);
  }
}
