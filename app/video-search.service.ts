import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Video } from './video';

@Injectable()
export class VideoSearchService {
  constructor(private http: Http) { }
  search(term: string): Observable<Video[]> {
    return this.http
      .get(`app/videos/?name=${term}`)
      .map((r: Response) => r.json().data as Video[])
      .catch((error: any) => {
          console.error('An friendly error occurred', error);
          return Observable.throw(error.message || error);
      });
  }
}
