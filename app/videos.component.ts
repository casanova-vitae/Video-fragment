import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Video } from './video';
import { VideoService } from './video.service';

@Component({
  moduleId: module.id,
  selector: 'my-videos',
  templateUrl: 'videos.component.html',
  styleUrls: ['videos.component.css']
})
export class VideosComponent implements OnInit {
  videos: Video[];
  selectedVideo: Video;
  addingVideo = false;
  error: any;

  constructor(
    private router: Router,
    private videoService: VideoService) { }

  getVideos(): void {
    this.videoService
      .getVideos()
      .then(videos => this.videos = videos)
      .catch(error => this.error = error);
  }

  addVideo(): void {
    this.addingVideo = true;
    this.selectedVideo = null;
  }

  close(savedVideo: Video): void {
    this.addingVideo = false;
    if (savedVideo) { this.getVideos(); }
  }

  deleteVideo(video: Video, event: any): void {
    event.stopPropagation();
    this.videoService
      .delete(video)
      .then(res => {
        this.videos = this.videos.filter(h => h !== video);
        if (this.selectedVideo === video) { this.selectedVideo = null; }
      })
      .catch(error => this.error = error);
  }

  ngOnInit(): void {
    this.getVideos();
  }

  onSelect(video: Video): void {
    this.selectedVideo = video;
    this.addingVideo = false;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedVideo.id]);
  }
}
