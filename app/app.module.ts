import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { CustomFormsModule } from 'ng2-validation'
import './rxjs-extensions';
import { AppComponent } from './app.component';
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { VideoService } from './video.service';
import { VideoSearchComponent } from './video-search.component';
import { TagInputModule } from 'ng2-tag-input';

@NgModule({
  //makes the exported declarations of other modules available in the current module
  imports: [
    TagInputModule,
    CustomFormsModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 600 })
  ],
  /*
   declarations are to make directives (including components and pipes) from the current module available to other directives in the current module.
   Selectors of directives, components or pipes are only matched against the HTML if they are declared or imported.
   */
  declarations: [
    AppComponent,
    VideoSearchComponent,
    routedComponents
  ],
  /*providers are to make services and values known to DI. They are added to the root scope and they are injected to other services or directives that have them as dependency.
  */
  providers: [
    VideoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
