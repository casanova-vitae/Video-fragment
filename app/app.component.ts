import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <div class="header-bar"></div>
    <nav>
      <a routerLink="/videos" routerLinkActive="active">Clip List</a> 
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['app.component.css'] //import css style
})
export class AppComponent {
  title = 'Video Fragment Tool';
}
