export class InMemoryDataService {
  //Tiny Bd for testing purpose
  createDb() {
    let videos = [
      { id: 0, name: 'Clip Savage', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 1, end: 52, tag:[]},
      { id: 1, name: 'Clip Savage', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 2, end: 4 , tag:['Javascript', 'Typescript']},
      { id: 2, name: 'Clip Honest', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 4, end: 5 , tag:[]},
      { id: 3, name: 'Clip Space', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 7, end: 9 ,  tag:[]},
      { id: 4, name: 'Clip Tiny', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 11, end: 12 , tag:[]},
      { id: 5, name: 'Clip Must', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 14, end: 18 , tag:[]}
    ];
    return { videos };
  }
}
