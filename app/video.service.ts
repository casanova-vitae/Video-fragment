import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Video } from './video';
/*
* This Service is An in-memory web api for Angular demos and tests. works intercepting HTTP request and returns
* the response object in a RESTy web api.
* */
@Injectable()//For creating "just-in-time" services
export class VideoService {
  private videosUrl = 'app/videos';  // URL to web api

  constructor(private http: Http) { }

  getVideos(): Promise<Video[]> {
    return this.http
      .get(this.videosUrl)
      .toPromise()
      .then(response => response.json().data as Video[])
      .catch(this.handleError);
  }

  getVideo(id: number): Promise<Video> {
    return this.getVideos()
      .then(videos => videos.find(video => video.id === id));
  }

  save(video: Video): Promise<Video> {
    if (video.id) {
      return this.put(video);
    }
    return this.post(video);
  }
  // Delete Video
  delete(video: Video): Promise<Response> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let url = `${this.videosUrl}/${video.id}`;
    return this.http
      .delete(url, { headers: headers })
      .toPromise()
      .catch(this.handleError);
  }

  // Add new Video
  private post(video: Video): Promise<Video> {
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http
      .post(this.videosUrl, JSON.stringify(video), { headers: headers })
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  // Update existing Video
  private put(video: Video): Promise<Video> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.videosUrl}/${video.id}`;

    return this.http
      .put(url, JSON.stringify(video), { headers: headers })
      .toPromise()
      .then(() => video)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
