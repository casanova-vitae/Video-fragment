import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Video } from './video';
import { VideoService } from './video.service';


@Component({
  moduleId: module.id,
  selector: 'my-video-detail',
  templateUrl: 'video-detail.component.html',
  styleUrls: ['video-detail.component.css'],

})

export class VideoDetailComponent implements OnInit {
  @Input() video: Video;
  @Output() close = new EventEmitter();
  error: any;
  navigated = false; // true if navigated here

  constructor(
    private videoService: VideoService,
    private route: ActivatedRoute) {
  }

  //Getting the selected video
  ngOnInit(): void {
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        let id = +params['id'];
        this.navigated = true;
        this.videoService.getVideo(id)
            .then(video => this.video = video);
      } else {
        this.navigated = false;
        this.video = new Video();
        this.video.url = "http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4";
      }
    });
  }

  save(): void {
    this.videoService
        .save(this.video)
        .then(video => {
          this.video = video; // saved video, w/ id if new
          this.goBack(video);
        })
        .catch(error => this.error = error); // TODO: Display error message
  }

  goBack(savedVideo: Video = null): void {
    this.close.emit(savedVideo);
    if (this.navigated) { window.history.back(); }
  }


}
