import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { VideoSearchService } from './video-search.service';
import { Video } from './video';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  moduleId: module.id,
  selector: 'video-search',
  templateUrl: 'video-search.component.html',
  styleUrls: ['video-search.component.css'],
  providers: [VideoSearchService]
})
export class VideoSearchComponent implements OnInit {
  videos: Observable<Video[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private videoSearchService: VideoSearchService,
    private router: Router) { }

  search(term: string): void {
    // Push a search term into the observable stream.
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.videos = this.searchTerms
      .debounceTime(300)        // wait for 300ms pause in events
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time
        // return the http search observable
        ? this.videoSearchService.search(term)
        // or the observable of empty videos if no search term
        : Observable.of<Video[]>([]))
      .catch(error => {
        // TODO: real error handling
        console.log(`Error in component ... ${error}`);
        return Observable.of<Video[]>([]);
      });
  }

  gotoDetail(video: Video): void {
    let link = ['/detail', video.id];
    this.router.navigate(link);
  }
}
