//Class Video definition
export class Video {
  id: number;
  name: string;
  url: string;
  start: number;
  end: number;
}
