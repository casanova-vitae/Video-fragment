# Getting Started


Git global setup

git config --global user.name "Marco Casanova"
git config --global user.email "casanova.vitae@gmail.com"

Create a new repository

git clone https://gitlab.com/casanova-vitae/Video-fragment.git
cd Video-fragment
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/casanova-vitae/Video-fragment.git
git add .
git commit
git push -u origin mast


#Pre Requisites node latest stable

Go via console to the folder and type:
npm start.
the app will display in port 8000
Enjoy!
